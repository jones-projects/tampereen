import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { latLng, MapOptions, tileLayer, Map, Marker, icon, LayerGroup, layerGroup } from 'leaflet';
import { HttpService } from '../service/http.service';
import { IDevices, IDeviceList, IDeviceListResource } from '../interfaces/IDevices';
import { CustomLeafletMarkerPin } from '../service/customLeafletMarkerPin';
import { ArgumentType } from '@angular/compiler/src/core';
import { AppService } from '../app.service';
@Component({
  selector: 'app-devices',
  templateUrl: './devices.component.html',
  styleUrls: ['./devices.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class DevicesComponent implements OnInit {
  private httpService: HttpService;
  map: Map;
  mapOptions: MapOptions;
  airSensorList: IDevices[];
  cameraList: any[];
  lightPoleList: IDevices[];
  CameraMarkerGroups;
  LightPoleMarkerGroups;
  AirSensorMarkerGroups;
  layersControl;
  private selectedDeveiceLabel: any;

  baseLayer = tileLayer(
    'http://{s}.google.com/vt/lyrs=s&x={x}&y={y}&z={z}',
    {
      maxZoom: 20,
      minZoom: 4,
      attribution: 'Qualcomm-Base',
      tileSize: 512,
      zoomOffset: -1,
      subdomains: ['mt0', 'mt1', 'mt2', 'mt3']
    });
  cameraLayer = tileLayer(
    'http://{s}.google.com/vt/lyrs=s&x={x}&y={y}&z={z}',
    {
      maxZoom: 20,
      minZoom: 4,
      attribution: 'Camera',
      tileSize: 512,
      zoomOffset: -1,
      subdomains: ['mt0', 'mt1', 'mt2', 'mt3']
    });


  // BlueAirQualityMarker = new Marker([61.504950, 23.743050])
  // .bindPopup('Normal Air Quality') 
  // .setIcon(icon({
  //         iconSize: [25, 41],
  //         iconAnchor: [13, 41],
  //         iconUrl: 'assets/marker-icon.png'
  //   }));;

  //  CameraMakerGroups=new LayerGroup();
  //   LightPoleMarkerGroups=new LayerGroup([this.BlueAirQualityMarker]);
  //  AirSensorMarkerGroups=new LayerGroup([this.BlueAirQualityMarker]);
  //   layersControl = {

  //     overlays: {
  //       'Cameras': this.CameraMakerGroups,
  //       'LightPoles':this.LightPoleMarkerGroups,
  //       'Air Sensors':this.AirSensorMarkerGroups
  //     }
  // };

  constructor(private appService: AppService) {
    // this.httpService = HttpService.Instance;
    // // this.map = Map.Instance;
    // this.httpService.requestPromiseGet('http://localhost:1334/assets/json/devices.json').then((devices: IDeviceListResource) => {
    //   this.appService.setDeviceArr(devices);
    // });
    // let selectedDeveiceLabel = this.appService.getSelectedCheck();
    // if (this.selectedDeveiceLabel === 'Air Sensors') {
    //   this.lightPoleList = this.appService.filterCameraArr();
    // } else if (this.selectedDeveiceLabel === 'Cameras') {
    //   this.cameraList = this.appService.filterCameraArr();
    // } else if (this.selectedDeveiceLabel === 'Light Poles') {
    //   this.airSensorList = this.appService.filterCameraArr();
    // }

    // console.log('this.lightPoleList', this.lightPoleList);
    // console.log(' this.cameraList', this.cameraList);
    // console.log(' this.airSensorList', this.airSensorList);
    // console.log('updateSelectedDeviceList',this.appService.updateSelectedDeviceList());
  }

  ngOnInit() {

    this.initializeMapOptions();
  }
  onMapReady(map: Map) {
    this.map = map;
    console.log('device.components::', map);
    this.renderMapData();
    ///this.addSampleMarker();

    //this.map.removeLayer(this.AirSensorMarkerGroups);
  }
  private initializeMapOptions() {
    this.mapOptions = {
      center: latLng(61.494333, 23.775472),
      zoom: 15,
      layers: [
        this.baseLayer,
        this.cameraLayer
      ],
    };

  }
  private renderMapData() {
    this.httpService = HttpService.Instance;
    this.httpService.requestPromiseGet('http://localhost:1334/assets/json/devices.json').then((devices: IDeviceListResource) => {
      // let selectedDeveiceLabel = this.appService.getSelectedCheck();

        this.cameraList = Object.values(devices.resourceCategory["Cameras"]);
        this.lightPoleList = Object.values(devices.resourceCategory["Light Poles"]);
        this.airSensorList = Object.values(devices.resourceCategory["Air Sensors"]);
      // console.log (this.cameraList[0])
      this.appService.setDeviceArr(devices);
      this.CameraMarkerGroups = new LayerGroup(
        this.cameraList.map(x =>
          new Marker([Number(x.deviceGPSLat), Number(x.deviceGPSLong)]
            , { icon: new CustomLeafletMarkerPin().seletedMarkerPin(x.deviceType, x.lastNotificationType) })
            .bindPopup(this.renderMarkerPopup(x)
            ).on('click', this.markerClick, this)
          .addTo(this.map)   
        ));
      this.LightPoleMarkerGroups = new LayerGroup(
        this.lightPoleList.map(x =>
          new Marker([Number(x.deviceGPSLat), Number(x.deviceGPSLong)]
            , { icon: new CustomLeafletMarkerPin().seletedMarkerPin(x.deviceType, x.lastNotificationType) })
            .bindPopup(this.renderMarkerPopup(x)
            ).on('click', this.markerClick, this)
          .addTo(this.map)   
        ));
      this.AirSensorMarkerGroups = new LayerGroup(
        this.airSensorList.map(x =>
          new Marker([Number(x.deviceGPSLat), Number(x.deviceGPSLong)]
            , { icon: new CustomLeafletMarkerPin().seletedMarkerPin(x.deviceType, x.lastNotificationType) })
            .bindPopup(this.renderMarkerPopup(x)
            ).on('click', this.markerClick, this)
          .addTo(this.map)   
        ));
      this.layersControl = {
        overlays: {
          'Cameras': this.CameraMarkerGroups,
          'LightPoles': this.LightPoleMarkerGroups,
          'Air Sensors': this.AirSensorMarkerGroups
        }
      };
    });

  }
  private markerClick() {
    console.log('device.components::');
  }
  private renderMarkerPopup(dev: any) {
    // console.log(dev);
    let data = '<section class="img-name-and-btn">'
      + '<div class="icnwarning">'
      + '<span class="material-icons">warning</span>'
      + '</div>'
      + '<label class="list-item-name map">'
      + dev.deviceLocationName
      + '</label>'
      + '<div class="list-item-image">'
      + '<span class="material-icons">videocam</span>'
      + '</div>'
      + '<div class="list-item-image">'
      + '<span class="material-icons">photo_library</span>'
      + '</div>'
      + '</section>'
      + '<section class="list-item-info">'
      + '<header>'
      + dev.lastNotificationMessage
      + '</header>'
      + '<address>'
      + dev.lastNotificationDetail
      + '</address>'
      + '<aside>'
      + '<button>'
      + dev.lastNotificationActionLabel
      + '</button>'
      + '<time>'
      + dev.lastNotificationTimestamp
      + '</time>'
      + '</aside>'
      + '</section>';
    return data;
  }
  private addSampleMarker() {
    const marker = new Marker([61.504967, 23.743065])
      .bindPopup('A pretty CSS3 popup.<br> Easily customizable.')
      .setIcon(
        icon({
          iconSize: [25, 41],
          iconAnchor: [13, 41],
          iconUrl: 'assets/marker-icon.png'
        }));
    marker.addTo(this.map);

    //Add Layers

    //   BlueAirQualityMarker.addTo(this.map);
    // denver    = new Marker([39.74, -104.99]).bindPopup('This is Denver, CO.'),
    // aurora    = new Marker([39.73, -104.8]).bindPopup('This is Aurora, CO.'),
    // golden    = new Marker([39.77, -105.23]).bindPopup('This is Golden, CO.');
  }
}
