import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListModule } from '../list/list.module';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { DevicesComponent } from './devices.component';

@NgModule({
  declarations: [DevicesComponent],
  imports: [
    CommonModule,
    ListModule,
    LeafletModule
  ]
})
export class DevicesModule { }
