import { Component, OnInit, Input, ViewEncapsulation, HostListener } from '@angular/core';
import { AppService } from 'src/app/app.service';

@Component({
    selector: 'app-sub-header',
    templateUrl: './sub.header.component.html',
    styleUrls: ['./sub.header.component.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class SubHeaderComponent implements OnInit {

    @Input() heatmapOptions: any;
    @Input() devicesOptions: any;
    @Input() alertsOptions: any;
    public deviceTypeList: any;
    public alertTypeList: any;
    
    public dropdownToShow: string;

    constructor(private appService: AppService) {
        this.dropdownToShow = '';
        this.deviceTypeList = {
            Cameras: { displayName: 'Cameras', actualName: 'Cameras' },
            Light_Poles: { displayName: 'Light Poles', actualName: 'Light Poles' },
            Air_Sensors: { displayName: 'Air Sensors', actualName: 'Air Sensors' }
        }

        this.alertTypeList = {
            Events: { displayName: 'Events', actualName: 'Events' },
            Incidents: { displayName: 'Incidents', actualName: 'Incidents' },
            Operational_Alerts: { displayName: 'Operational Alerts', actualName: 'Operational Alerts' }
        }
    }

    @HostListener('document:click', ['$event'])
    clickout(event) {
        let clickedElement = event.target;
        let activeDropDownList = clickedElement.closest('.drop-down-list.show');
        let dropDownList = clickedElement.closest('.drop-down-list');
        // console.log(activeDropDownList);
        // console.log(dropDownList);
        if (activeDropDownList) {
            this.dropdownToShow = '';
        }
        event.stopPropagation();
    }

    ngOnInit() {
    }

    public showDropdown(name: string) {
        // setTimeout(() => {
            this.dropdownToShow = name;
        // }, 10);
    }

    public onChangeOption(event) {
        let elementId = event.target.id;
        if (this.deviceTypeList[elementId]) {
            console.log(this.deviceTypeList[elementId].actualName);
            this.appService.setSelectedCheck(this.deviceTypeList[elementId].actualName);
        } else if (this.alertTypeList[elementId]) {
            console.log(this.alertTypeList[elementId].actualName);
            this.appService.setSelectedCheck(this.alertTypeList[elementId].actualName)
        }
    }

}
