import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Sub.HeaderComponent } from './sub.header.component';

describe('Sub.HeaderComponent', () => {
  let component: Sub.HeaderComponent;
  let fixture: ComponentFixture<Sub.HeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Sub.HeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Sub.HeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
