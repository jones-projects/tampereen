import { Component, OnInit, ViewEncapsulation, HostListener} from '@angular/core';
import {latLng, MapOptions, tileLayer, Map, Marker, icon,LayerGroup, } from 'leaflet';
import { HttpService } from '../service/http.service';
import { INotificationList, INotification} from '../interfaces/INotification';
import {CustomLeafletMarkerPin }from '../service/customLeafletMarkerPin';
@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class NotificationsComponent implements OnInit {

  map: Map;
  mapOptions: MapOptions;
  private httpService: HttpService;

  public notifyList:INotification[];

  private cameraMarkers;
  


            
 
   baseLayer=tileLayer(
          'http://{s}.google.com/vt/lyrs=s&x={x}&y={y}&z={z}',
          {
            maxZoom: 20,
            minZoom:4,
            attribution: 'Qualcomm-Base',
            tileSize: 512, 
            zoomOffset: -1,
            subdomains:['mt0','mt1','mt2','mt3']
          });
      cameraLayer= tileLayer(
            'http://{s}.google.com/vt/lyrs=s&x={x}&y={y}&z={z}',
            {
              maxZoom: 20,
              minZoom:4,
              attribution: 'Camera',
              tileSize: 512, 
              zoomOffset: -1,
              subdomains:['mt0','mt1','mt2','mt3']
            });
            
          layersControl = {
      
              overlays: {
                'Cameras': this.cameraMarkers,
                /*Add multiple layers here*/
              }
            };
            

  constructor() {
    
   }

  ngOnInit() {
    this.initializeMapOptions();
  }
  onMapReady(map: Map) {
    console.log("map on ready")
    this.map = map;
    this.renderMapData();
   // this.addSampleMarker();
   
    
  }
  private initializeMapOptions() {
   
    this.mapOptions = {
      center: latLng(61.494333, 23.775472),
      zoom: 15,
      layers: [
        this.baseLayer,
        this.cameraLayer
         
      ],
    };
  }
  private renderMapData(){
  this.httpService = HttpService.Instance;
    this.httpService.requestPromiseGet('http://localhost:1334/assets/json/notifications.json').then((notifications: INotificationList) => {
       // console.log('notificatioins::', notifications);
        this.notifyList=Object.values(notifications.Notifications);
      // console.log(this.notifyList)
    

    //assingn Lat-Lon to markers
     
    this.cameraMarkers=new LayerGroup(
      this.notifyList.map(x=> 
            new Marker([Number(x.deviceGPSLat), Number(x.deviceGPSLong)]
            ,{icon:new CustomLeafletMarkerPin().seletedMarkerPin(x.deviceType,x.NotificationType)})
            .bindPopup(    this.renderMarkerPopup(x)         
            ).on('click', this.markerClick, this).addTo(this.map)
        ));

        //Set Base layers
      
          //latlng: latLng([61.493568, 23.776563])
         // new Marker([61.493568, 23.776563]).fire("click");
 
  });
}

private renderMarkerPopup(notifi:INotification)
{
  console.log(notifi);
 let data='<section class="img-name-and-btn">'
            +'<div class="icnwarning">'
              +'<span class="material-icons">warning</span>'
            +'</div>'
            +'<label class="list-item-name map">'
               +notifi.NotificationHeader
            +'</label>'
            +'<div class="list-item-image">'
                +'<span class="material-icons">videocam</span>'
            +'</div>'
            +'<div class="list-item-image">'
              +'<span class="material-icons" (click)="handleClickImageOverlay">photo_library</span>'
            +'</div>'
          +'</section>'
          +'<section class="list-item-info">'
            +'<header>'
              +notifi.LocationName
            +'</header>'
            +'<address>'
              +notifi.Address
              +'</address>'
            +'<aside>'
            +'<button>'
             + notifi.NotificationActionLabel
            +'</button>'
            +'<time>'
              +notifi.NotificationTimestamp
             +'</time>'
            +'</aside>'
            +'</section>';
  return data;
}
private markerClick(e) {
 
}

  private addSampleMarker() {
    const marker = new Marker([61.494333, 23.775472])
    .bindPopup('A pretty CSS3 popup.<br> Easily customizable.')
      .setIcon(
        icon({
          iconSize: [25, 41],
          iconAnchor: [13, 41],
          iconUrl: 'assets/marker-icon.png'
        }));
    marker.addTo(this.map);

    //Add Layers
   
                         //   BlueAirQualityMarker.addTo(this.map);
    // denver    = new Marker([39.74, -104.99]).bindPopup('This is Denver, CO.'),
    // aurora    = new Marker([39.73, -104.8]).bindPopup('This is Aurora, CO.'),
    // golden    = new Marker([39.77, -105.23]).bindPopup('This is Golden, CO.');
  }
}