import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NotificationsComponent } from './notifications.component';
import {ListModule} from '../list/list.module';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { OverlayModule } from '../common/overlay/overlay.module';
import { PipeModule } from '../pipe/pipe.module';
@NgModule({
  declarations: [NotificationsComponent],
  imports: [
    CommonModule,
    ListModule,
    LeafletModule,
    OverlayModule,
		PipeModule
  ]
})
export class NotificationsModule { }
