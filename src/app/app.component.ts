import { Component, ViewEncapsulation } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class AppComponent {
    
    private routedTabName: string;

    constructor(private router: Router) { 
        this.routedTabName = '';
        this.router.events.subscribe(this.routerChangelistener.bind(this));
    }

    private routerChangelistener(event: Event) {
        if (event instanceof NavigationEnd) {
            const { url } = event;
            // let moduleName = url.split('/')[1];
            let moduleName = url.substr(1);
            if (moduleName) {
                this.routedTabName = moduleName;
            }
        }
    }

    public handleRouterNameEvent(routeName: string) {
        this.router.navigate([`/${routeName}`]);
    }

    public handleShowCamera(event) {
        console.log(event);
    }

}
