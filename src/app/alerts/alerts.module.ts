import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AlertsComponent } from './alerts.component';

import {ListModule} from '../list/list.module';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';

@NgModule({
  declarations: [AlertsComponent],
  imports: [
    CommonModule,
    ListModule,
    LeafletModule
  ]
})
export class AlertsModule { }
