import { Component, OnInit } from '@angular/core';
import {latLng, MapOptions, tileLayer, Map, Marker, icon,LayerGroup, layerGroup} from 'leaflet';
@Component({
  selector: 'app-alerts',
  templateUrl: './alerts.component.html',
  styleUrls: ['./alerts.component.scss']
})
export class AlertsComponent implements OnInit {

  map: Map;
  mapOptions: MapOptions;

  baseLayer=tileLayer(
    'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
    {
      maxZoom: 14,
      minZoom:4,
      attribution: 'Qualcomm-Base',
      tileSize: 512, 
      zoomOffset: -1,
     
    });
    cameraLayer= tileLayer(
      'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
      {
        maxZoom: 14,
        minZoom:4,
        attribution: 'Camera',
        tileSize: 512, 
        zoomOffset: -1,

      });
      
            
      BlueAirQualityMarker = new Marker([61.504950, 23.743050])
      .bindPopup('Normal Air Quality') 
      .setIcon(icon({
              iconSize: [25, 41],
              iconAnchor: [13, 41],
              iconUrl: 'assets/marker-icon.png'
        }));;
      
        cameraMarkers=new LayerGroup([this.BlueAirQualityMarker]);
      layersControl = {
      
        overlays: {
          'Cameras': this.cameraMarkers,
          /*Add multiple layers here*/
        }
      };

  constructor() { }

  ngOnInit() {
    this.initializeMapOptions();
  }
  onMapReady(map: Map) {
    this.map = map;
   // this.addSampleMarker();
  }
  private initializeMapOptions() {
   
    this.mapOptions = {
      center: latLng(61.504967, 23.743065),
      zoom: 14,
      layers: [
        this.baseLayer,
        this.cameraLayer
         
      ],
    };
  }

  private addSampleMarker() {
    const marker = new Marker([61.504967, 23.743065])
    .bindPopup('A pretty CSS3 popup.<br> Easily customizable.')
      .setIcon(
        icon({
          iconSize: [25, 41],
          iconAnchor: [13, 41],
          iconUrl: 'assets/marker-icon.png'
        }));
    marker.addTo(this.map);

    //Add Layers
   
                         //   BlueAirQualityMarker.addTo(this.map);
    // denver    = new Marker([39.74, -104.99]).bindPopup('This is Denver, CO.'),
    // aurora    = new Marker([39.73, -104.8]).bindPopup('This is Aurora, CO.'),
    // golden    = new Marker([39.77, -105.23]).bindPopup('This is Golden, CO.');
  }
}