import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable()
export class AppService {

    private allDeviceList: any;
    private filterCamaraList: any[];
    private filterLightPoleList: any[];
    private filterSensorList: any[];
    private selectedDeviceList: any[];

    private deviceNameList: any[];
    private deviceListSubject: Subject<any[]>;


    constructor() {
        this.allDeviceList = [];
        this.filterCamaraList = [];
        this.filterLightPoleList = [];
        this.filterSensorList = [];
        this.selectedDeviceList = [];
        this.deviceNameList = [];
        this.deviceListSubject = new Subject<any[]>();
    }

    public filterCameraArr() {
        if (this.allDeviceList.resourceCategory["Cameras"]) {
            this.filterCamaraList = Object.values(this.allDeviceList.resourceCategory["Cameras"]);
            console.log('this.filterCamaraList ', this.filterCamaraList);
        }
        return this.filterCamaraList;
    }

    public filterLightPoleArr() {
        if (this.allDeviceList.resourceCategory["Light Poles"]) {
            this.filterLightPoleList = Object.values(this.allDeviceList.resourceCategory["Light Poles"]);
        }
        return this.filterLightPoleList;
    }

    public filterSensorArr() {
        if (this.allDeviceList.resourceCategory["Air Sensors"]) {
            this.filterSensorList = Object.values(this.allDeviceList.resourceCategory["Air Sensors"]);
        }
        return this.filterSensorList;
    }

    public setDeviceArr(value: any) {
        this.allDeviceList = value;
    }

    public getDeviceArr() {
        return this.allDeviceList;
    }

    public setSelectedCheck(value: any) {
        let selectedCamera = this.deviceNameList.find((camera) => camera === value);
        if (selectedCamera) {
            // remove it
            let index = this.deviceNameList.indexOf(value);
            this.deviceNameList.splice(index, 1);
        } else {
            // add it
            this.deviceNameList.push(value);
        }
        this.deviceListSubject.next(this.deviceNameList);
    }

    public getSelectedCheck() {
        return this.selectedDeviceList;
    }

    public observeDeviceList(): Subject<any[]> {
        return this.deviceListSubject;
    }

}