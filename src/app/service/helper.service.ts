export class HelperService {

    public static isJSON(data: any) {
        try {
            JSON.parse(data);
        } catch (e) {
            return false;
        }
        return true;
    }

    public static convertToCamelCases(data: any): string {
        const formatedData = JSON.stringify(data).
            replace('Light Poles', 'lightPoles').
            replace('Cameras', 'cameras').
            replace('deviceID', 'deviceId').
            replace('Air Sensors', 'airSensors');
        return formatedData;
    }

    public static processedXHRResponse(xhr: XMLHttpRequest) {
        if (this.isJSON(xhr.responseText)) {
            let jsonResponse = JSON.parse(xhr.responseText);

            for (const iKey in jsonResponse) {
                if (jsonResponse.hasOwnProperty(iKey)) {
                    const kValue = jsonResponse[iKey];
                    for (const jkey in kValue) {
                        if (kValue.hasOwnProperty(jkey)) {
                            kValue[jkey] = Object.assign({}, JSON.parse(this.convertToCamelCases(kValue[jkey])));
                        }
                    }
                }
            }

            return jsonResponse;
        }
    }

}