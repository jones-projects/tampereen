import {icon}  from 'leaflet';


export class CustomLeafletMarkerPin{
    cameraBlueIcon = icon({
        iconUrl:'../../assets/map-pins/Blue - Camera.svg',
        shadowUrl: '',
        iconSize:     [25, 30],
        shadowSize:   [15, 30],
        iconAnchor:   [15, 30],
        shadowAnchor: [4, 62],
        popupAnchor:  [-5, -30]
      });
      cameraOrangeIcon = icon({
        iconUrl:'../../assets/map-pins/Orange - Camera.svg',
        shadowUrl: '',
        iconSize:     [25, 30],
        shadowSize:   [15, 30],
        iconAnchor:   [15, 30],
        shadowAnchor: [4, 62],
        popupAnchor:  [-5, -30]
      });
      cameraRedIcon = icon({
        iconUrl:'../../assets/map-pins/Red - Camera.svg',
        shadowUrl: '',
        iconSize:     [25, 30],
        shadowSize:   [15, 30],
        iconAnchor:   [15, 30],
        shadowAnchor: [4, 62],
        popupAnchor:  [-5, -30]
       });
        airQualityBlueIcon=icon({
          iconUrl:'../../assets/map-pins/Blue - Air Quality.svg',
          shadowUrl: '',
          iconSize:     [25, 30],
          shadowSize:   [15, 30],
          iconAnchor:   [15, 30],
          shadowAnchor: [4, 62],
          popupAnchor:  [-5, -30]
      });
      airQualityOrangeIcon=icon({
        iconUrl:'../../assets/map-pins/Orange - Air Quality.svg',
        shadowUrl: '',
        iconSize:     [25, 30],
        shadowSize:   [15, 30],
        iconAnchor:   [15, 30],
        shadowAnchor: [4, 62],
        popupAnchor:  [-5, -30]
    });
    airQualityRedIcon=icon({
      iconUrl:'../../assets/map-pins/Red - Air Quality.svg',
      shadowUrl: '',
      iconSize:     [25, 30],
      shadowSize:   [15, 30],
      iconAnchor:   [15, 30],
      shadowAnchor: [4, 62],
      popupAnchor:  [-5, -30]
  });
  lightPoleBlueIcon=icon({
    iconUrl:'../../assets/map-pins/Blue - Light Pole.svg',
    shadowUrl: '',
    iconSize:     [25, 30],
    shadowSize:   [15, 30],
    iconAnchor:   [15, 30],
    shadowAnchor: [4, 62],
    popupAnchor:  [-5, -30]
});
lightPoleOrangeIcon=icon({
  iconUrl:'../../assets/map-pins/Orange - Light Pole.svg',
  shadowUrl: '',
  iconSize:     [25, 30],
  shadowSize:   [15, 30],
  iconAnchor:   [15, 30],
  shadowAnchor: [4, 62],
  popupAnchor:  [-5, -30]
});
lightPoleRedIcon=icon({
iconUrl:'../../assets/map-pins/Red - Light Pole.svg',
shadowUrl: '',
iconSize:     [25, 30],
shadowSize:   [15, 30],
iconAnchor:   [15, 30],
shadowAnchor: [4, 62],
popupAnchor:  [-5, -30]
});
defaultIcon=icon({
    iconUrl:'assets/marker-icon.png',
    shadowUrl: '',
    iconSize:     [25, 30],
    shadowSize:   [15, 30],
    iconAnchor:   [15, 30],
    shadowAnchor: [4, 62],
    popupAnchor:  [-5, -30]
    });
public seletedMarkerPin(deviceType:string,notificationType:string){
    if(deviceType=="Camera" && (notificationType=="Incident"|| notificationType=="Events"))
        return this.cameraBlueIcon;
     else if(deviceType=="Camera" && notificationType=="Operational Alerts")
        return this.cameraOrangeIcon;
    else if((deviceType=="Light Poles" || deviceType== "lightPoles")&& (notificationType=="Incident" || notificationType=="Events"))
        return this.lightPoleBlueIcon;    
    else if((deviceType=="Light Poles" || deviceType== "lightPoles") && notificationType=="Operational Alerts")
        return this.lightPoleOrangeIcon;
    else if((deviceType=="Air Sensors" || deviceType== "airSensors") && (notificationType=="Incident" || notificationType=="Events"))
        return this.airQualityBlueIcon;    
    else if((deviceType=="Air Sensors" || deviceType== "airSensors") && notificationType=="Operational Alerts")
        return this.airQualityOrangeIcon;
    else if((deviceType=="Air Sensors" || deviceType== "airSensors") && notificationType=="Alerts")
      return this.airQualityRedIcon;
     else    
     {
         console.log(deviceType,notificationType)
     }
        return this.defaultIcon;

}
}