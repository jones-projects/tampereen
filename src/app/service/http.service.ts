import { HelperService } from './helper.service';

export class HttpService {

    private static instance: HttpService;

    constructor() { }

    public static get Instance() {
        if (this.instance === null || this.instance === undefined) {
            this.instance = new HttpService();
        }
        return this.instance;
    }

    public requestPromiseGet<T>(url: string): Promise<T> {

        return new Promise<T>((resolve, reject) => {
            const xhr = new XMLHttpRequest();
            xhr.onreadystatechange = (event: any) => {
                if (xhr.readyState === 4) {
                    if (xhr.status >= 200) {
                        let processedResponse = HelperService.processedXHRResponse(xhr);
                        console.log('response::', processedResponse);
                        resolve(processedResponse);
                    }
                }
            };
            xhr.open('get', url, true);
            xhr.send();
        })
    }

}