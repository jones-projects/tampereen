import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { HttpService } from '../service/http.service';
import { latLng, MapOptions, tileLayer, Map, Marker, icon, LayerGroup, } from 'leaflet';
import { IDevices, IDeviceListResource } from '../interfaces/IDevices';
import { CustomLeafletMarkerPin } from '../service/customLeafletMarkerPin';
import { Router, NavigationEnd } from '@angular/router';
declare var L;
declare var HeatmapOverlay;

@Component({
    selector: 'app-heatmap',
    templateUrl: './heatmap.component.html',
    styleUrls: ['./heatmap.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class HeatmapComponent implements OnInit {

    private httpService: HttpService;
    map: Map;
    //mapOptions: L.MapOptions;
    airSensorList: IDevices[];
    cameraList: IDevices[];
    lightPoleList: IDevices[];
    allDeviceList: IDevices[];
    heatmapLayer;
    options: L.MapOptions;
    mapMarkerGroup;
    layersControl;
    data = {
        data: [
            { lat: 61.490429, lng: 23.75450, count: 50 },
            { lat: 61.59370, lng: 23.76119, count: 90 },
            { lat: 61.49390, lng: 23.76149, count: 90 },
            { lat: 61.49470, lng: 23.77219, count: 90 },
            { lat: 61.70019, lng: 23.72141, count: 70 },
            { lat: 61.49968, lng: 23.7941, count: 40 },
        ]
    };

    private showExpandBtnProp: boolean;
    public selectedItemInList: IDevices;

    public selectedListToShow: any;



    constructor(private router: Router) {
        // this.routedTabName = '';
        this.router.events.subscribe(this.routerChangelistener.bind(this));
        this.showExpandBtnProp = false;
        this.selectedItemInList = <IDevices>{};

        this.selectedListToShow = {
            cameras: true,
            lightPoleList: false,
            airSensorList: false
        }
    }

    ngOnInit() {
        this.heatmapLayer = new HeatmapOverlay({
            radius: 0.004,
            maxOpacity: 0.8,
            minOpacity: 0,
            scaleRadius: true,
            useLocalExtrema: true,
            latField: 'lat',
            lngField: 'lng',
            valueField: 'count',
        });
        this.options = {
            layers: [
                L.tileLayer('http://{s}.google.com/vt/lyrs=s&x={x}&y={y}&z={z}', {
                    maxZoom: 20,
                    minZoom: 4,
                    attribution: 'Qualcomm-Base',
                    tileSize: 512,
                    zoomOffset: -1,
                    subdomains: ['mt0', 'mt1', 'mt2', 'mt3'],
                }),
                this.heatmapLayer
            ],
            zoom: 14,
            center: L.latLng([61.494333, 23.775472])
        };
    }


    private routerChangelistener(event: Event) {
        if (event instanceof NavigationEnd) {
            const { url } = event;
            // let moduleName = url.split('/')[1];
            let moduleName = url.substr(1);
            if (moduleName) {
                console.log('module name', moduleName);
                // this.routedTabName = moduleName;
                // if(this.map!=undefined){
                //   document.getElementById("map").outerHTML = "";this.map=null;
                // }
                if (this.map != undefined) {
                    this.map.removeLayer(this.heatmapLayer);
                }
                if (moduleName == "heatmaps/Illumination") {

                    this.heatmapLayer = new HeatmapOverlay({
                        radius: 0.004,
                        maxOpacity: 0.8,
                        minOpacity: 0,
                        scaleRadius: true,
                        useLocalExtrema: true,
                        latField: 'lat',
                        lngField: 'lng',
                        valueField: 'count',


                    });


                }
                else if (moduleName == "heatmaps/Air_Quality") {
                    this.heatmapLayer = new HeatmapOverlay({
                        radius: 0.008,
                        maxOpacity: 0.8,
                        minOpacity: 0,
                        scaleRadius: true,
                        useLocalExtrema: true,
                        latField: 'lat',
                        lngField: 'lng',
                        valueField: 'count',


                    });
                }
                else if (moduleName == "heatmaps/Safety_Score") {
                    this.heatmapLayer = new HeatmapOverlay({
                        radius: 0.010,
                        maxOpacity: 0.8,
                        minOpacity: 0,
                        scaleRadius: true,
                        useLocalExtrema: true,
                        latField: 'lat',
                        lngField: 'lng',
                        valueField: 'count',


                    });
                }
                else if (moduleName == "heatmaps/Trafic_Congestio") {
                    this.heatmapLayer = new HeatmapOverlay({
                        radius: 0.007,
                        maxOpacity: 0.8,
                        minOpacity: 0,
                        scaleRadius: true,
                        useLocalExtrema: true,
                        latField: 'lat',
                        lngField: 'lng',
                        valueField: 'count',
                        gradient: {
                            // enter n keys between 0 and 1 here
                            // for gradient color customization
                            '.5': 'red  ',
                            '.8': 'green',
                            '.30': 'blue'
                        },

                    });
                } else if (moduleName == "heatmaps" || moduleName == "heatmaps/default") {
                  
                  //  this.map.removeLayer(this.heatmapLayer);
                    
                }
                if (this.map != undefined && moduleName != "heatmaps" && moduleName != "heatmaps/default") {
                  
                    this.map.addLayer(this.heatmapLayer);
                    this.heatmapLayer.setData(this.data);
                }
            }
        }
    }



    onMapReady(map) {
        this.map = map;
        // map.on('click', (event: L.LeafletMouseEvent) => {
        //   // debugger
        //   //   this.data.data.push({
        //   //     lat: event.latlng.lat,
        //   //     lng: event.latlng.lng,
        //   //     count: -100
        //   //   });

        //   //   this.heatmapLayer.setData(this.data);
        //   console.log(event.latlng.lat,event.latlng.lng);
        //   });
        this.renderMapData();
        this.heatmapLayer.setData(this.data);

    }

    private renderMapData() {
        this.httpService = HttpService.Instance;
        this.httpService.requestPromiseGet('http://localhost:1334/assets/json/devices.json').then((devices: IDeviceListResource) => {
            this.cameraList = Object.values(devices.resourceCategory["Cameras"]);
            this.lightPoleList = Object.values(devices.resourceCategory["Light Poles"]);
            this.airSensorList = Object.values(devices.resourceCategory["Air Sensors"]);
            this.allDeviceList = this.cameraList.concat(this.lightPoleList.concat(this.airSensorList));

            this.mapMarkerGroup = new LayerGroup(
                this.allDeviceList.map(x =>
                    new L.Marker([Number(x.deviceGPSLat), Number(x.deviceGPSLong)]
                        , { icon: new CustomLeafletMarkerPin().seletedMarkerPin(x.deviceType, x.lastNotificationType) })
                        .bindPopup(this.renderMarkerPopup(x)
                        ).on('click', this.markerClick, this)
                        .addTo(this.map)
                ));
            // this.allDeviceList.map(x=>this.data.data.push({
            //   lat: x.deviceGPSLat,
            //   lng: x.deviceGPSLong,
            //   count: Math.floor(Math.random() * -100) + 1
            // }));
            // this.heatmapLayer.setData(this.data);
        });

    }

    private markerClick(event) {
        if (event && event.latlng) {
            const { lat, lng } = event.latlng;
            for(let item in this.allDeviceList) {
                let device = this.allDeviceList[item];
                if (device.deviceGPSLat == lat && device.deviceGPSLong == lng) {
                    this.handleClickOnListItem(device);
                }
            }
        }
    }
    

    private renderMarkerPopup(dev: any) {
        let data = '<section class="img-name-and-btn">'
            + '<div class="icnwarning">'
            + '<span class="material-icons">warning</span>'
            + '</div>'
            + '<label class="list-item-name map" '
            + 'title="' + dev.deviceLocationName + '">'
            + dev.deviceLocationName
            + '</label>'
            + '<div class="list-item-image">'
            + '<span class="material-icons">videocam</span>'
            + '</div>'
            + '<div class="list-item-image">'
            + '<span class="material-icons">photo_library</span>'
            + '</div>'
            + '</section>'
            + '<section class="list-item-info">'
            + '<header>'
            + dev.lastNotificationMessage
            + '</header>'
            + '<address>'
            + dev.lastNotificationDetail
            + '</address>'
            + '<aside>'
            + '<button>'
            + dev.lastNotificationActionLabel
            + '</button>'
            + '<time>'
            + dev.lastNotificationTimestamp
            + '</time>'
            + '</aside>'
            + '</section>';
        return data;
    }

    private showPopupOnMap(list: IDevices) {
        let getMapMarkerLayers = this.mapMarkerGroup.getLayers();
        for (let index = 0; index < getMapMarkerLayers.length - 1; index++) {
            const mapMarkerLayers = getMapMarkerLayers[index];
            let {lat, lng} = mapMarkerLayers.getLatLng();
            if (list.deviceGPSLat == lat && list.deviceGPSLong == lng) {
                // let eventList = getMapMarkerLayers[0].openPopup();
                mapMarkerLayers.openPopup();
            }
        }
    }

    public handleClickOnListItem(list: IDevices, selectedList?: string) {
        
        if (this.selectedItemInList['DeviceID'] !== list['DeviceID']) {
            console.log(list);
            this.showPopupOnMap(list);
            this.selectedItemInList = list;
            return;
        }
        
        this.selectedItemInList = <IDevices> {}
    }

    public expandCollapseList(item) {
        this.selectedListToShow[item] = !this.selectedListToShow[item];
        for (let listItem in this.selectedListToShow) {
            this.selectedListToShow[listItem] = (listItem === item);
        }
    }

}