import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeatmapComponent } from './heatmap.component';
import {ListModule} from '../list/list.module';
import {LeafletModule} from '@asymmetrik/ngx-leaflet';
import { ListItemModule } from '../list/list.item/list.item.module';
import { PipeModule } from '../pipe/pipe.module';


@NgModule({
  declarations: [HeatmapComponent],
  imports: [
    CommonModule,
    ListModule,
    LeafletModule,
    ListItemModule,
    PipeModule
  ]
})
export class HeatmapModule { }
