import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';  

import { AppComponent } from './app.component';

import { ListComponent } from './list/list.component';

import { AppRoutingModule } from './app-routing.module';
import { MasterHeaderModule } from './master.header/master.header.module';

import { ProgressChartModule } from './common/progress.chart/progress.chart.module';
import { LineChartModule } from './common/line.chart/line.chart.module';
import { BarChartModule } from './common/bar.chart/bar.chart.module';
import { ChartsModule } from 'ng2-charts';

import {HeatmapModule} from './heatmap/heatmap.module';
import {DevicesModule} from './devices/devices.module';
import {AlertsModule} from './alerts/alerts.module';
import { NotificationsModule } from './notifications/notifications.module';
import { AppService } from './app.service';
import { DeviceModule } from './device/device.module';
import { SubHeaderModule } from './header/sub.header/sub.header.module';

const APP_MODULES = [

    MasterHeaderModule,
 
    ProgressChartModule,
    LineChartModule,
    BarChartModule,

    HeatmapModule,
    DevicesModule,

    DeviceModule,

    AlertsModule,
    NotificationsModule,

    SubHeaderModule

];

const EXTERNAL_LIBRARY_MODULE = [

];

@NgModule({
    declarations: [
        AppComponent,
        
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        MasterHeaderModule,
        CommonModule,
        ChartsModule,
        
        APP_MODULES,
        
    ],
    providers: [AppService],
    bootstrap: [AppComponent]
})
export class AppModule { }
