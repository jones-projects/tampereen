import { Component, OnInit } from '@angular/core';
import { Label, MultiDataSet } from 'ng2-charts';
import { ChartType } from 'chart.js';

@Component({
    selector: 'app-progress-chart',
    templateUrl: './progress.chart.component.html',
    styleUrls: ['./progress.chart.component.scss']
})
export class ProgressChartComponent implements OnInit {

    // public doughnutChartLabels: Label[] = ['Mail-Order Sales', 'In-Store Sales' ];
    public doughnutChartLabels: Label[] = [];
    public doughnutChartData: MultiDataSet = [
        // [350, 450, 100],
        // [50, 150, 120],
        [30, 70]
    ];
    public doughnutChartType: ChartType = 'doughnut';

    constructor() { }

    ngOnInit() {
    }

    // events
    public chartClicked({ event, active }: { event: MouseEvent, active: {}[] }): void {
        console.log(event, active);
    }

    public chartHovered({ event, active }: { event: MouseEvent, active: {}[] }): void {
        console.log(event, active);
    }

}
