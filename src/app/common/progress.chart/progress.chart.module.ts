import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProgressChartComponent } from './progress.chart.component';
import { ChartsModule } from 'ng2-charts';

@NgModule({

	imports: [
		CommonModule,
		ChartsModule
	],
	declarations: [
		ProgressChartComponent
	],
	exports: [
		ProgressChartComponent
	]

})

export class ProgressChartModule { }