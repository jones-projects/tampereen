import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OverlayComponent } from './overlay.component';
import { PipeModule } from 'src/app/pipe/pipe.module';
@NgModule({

	imports: [
		CommonModule,
		PipeModule
	],
	declarations: [
		OverlayComponent
	],
	exports: [
		OverlayComponent
	]

})

export class OverlayModule { }