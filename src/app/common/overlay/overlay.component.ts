import { Component, OnInit, EventEmitter, Output, ViewEncapsulation, Input, HostListener } from '@angular/core';
@Component({
    selector: 'app-overlay',
    templateUrl: './overlay.component.html',
    styleUrls: ['./overlay.component.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class OverlayComponent implements OnInit {

    @Input() overlayBGColorProp: string;
    @Input() showOverlayProp: boolean;

    @Output() onEscapeEvent: EventEmitter<any>;
    @Output() onClickEvent: EventEmitter<any>;

    constructor() {
        this.onEscapeEvent = new EventEmitter<any>();
        this.onClickEvent = new EventEmitter<any>();
    }

    ngOnInit() {
    }

    @HostListener('document:keydown.escape', ['$event']) 
    public onKeydownHandler(event: KeyboardEvent) {
        this.onEscapeEvent.emit('escapeEvent');
    }

    public handleClickEvent(event) {
        this.onClickEvent.emit('clickEvent');
    }

}
