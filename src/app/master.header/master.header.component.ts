import { Component, OnInit, EventEmitter, Output, ViewEncapsulation, Input } from '@angular/core';
import { $ } from 'protractor';
import { AppService } from '../app.service';
@Component({
    selector: 'app-master-header',
    templateUrl: './master.header.component.html',
    styleUrls: ['./master.header.component.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class MasterHeaderComponent implements OnInit {

    @Input() routedTabNameProp: string;
    @Output() routerNameEvent: EventEmitter<string>;
    @Output() showCameraEvent: EventEmitter<any>;
    public notificationTypeList: any;
    public deviceTypeList: any;
    public deviceStatus={
        cameras:true,
        lightPoles:true,
        airSensors:true
    };
    constructor(private appService: AppService) {
        this.routerNameEvent = new EventEmitter<string>();
        this.showCameraEvent = new EventEmitter<any>();
        this.notificationTypeList = {
            Illumination: { displayName: 'Illumination', actualName: 'Illumination' },
            Safety_Score: { displayName: 'Safety Score', actualName: 'Safety Score' },
            Trafic_Congestio: { displayName: 'Trafic Congestion', actualName: 'Trafic Congestion' },
            Air_Quality: { displayName: 'Air Quality', actualName: 'Air Quality' },
        }
        this.deviceTypeList = {
            Cameras: { displayName: 'Cameras', actualName: 'Cameras' },
            Light_Poles: { displayName: 'Light Poles', actualName: 'Light Poles' },
            Air_Sensors: { displayName: 'Air Sensors', actualName: 'Air Sensors' }
        }

    }

    ngOnInit() {
    }

    public navigateTo(routeName: string) {
        this.routerNameEvent.emit(routeName);
    }

    public onChangeNotificationType(event) {
        let elementId = event.target.id;
        let deviceTypeName = this.notificationTypeList[elementId].actualName;
        let elementChecked = event.target.checked;
        this.routerNameEvent.emit('heatmaps/' + elementId);
        // this.addOrRemoveNotificationsByName(deviceTypeName);
    }

    public onChangeDeviceType(event) {
        let elementId = event.target.id;
        let deviceTypeName = this.deviceTypeList[elementId].actualName;
        let elementChecked = event.target.checked;
        // if(elementChecked) {
            this.appService.setSelectedCheck(deviceTypeName);
        // }
    }

    
    public selectDevice(e:any,val:string){
        this.appService.setSelectedCheck(val);
        let getDeviceList = this.appService.getDeviceArr();
        
       if(val=="Camera"){
           if(e.target.checked===true)
           {
               //$('')
           }
           else{

           }
        }
        console.log(this.deviceStatus)
    }

}
