import { NgModule } from '@angular/core';
import { MasterHeaderComponent } from "./master.header.component";
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';

@NgModule({

	imports: [
		CommonModule,HttpClientModule
	],
	declarations: [
		MasterHeaderComponent
	],
	exports: [
		MasterHeaderComponent
	]

})

export class MasterHeaderModule { }