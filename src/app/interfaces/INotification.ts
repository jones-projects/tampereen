
    export interface INotificationList{
        Notifications:INotification[]
    }
    export interface INotification{
        NotificationID:string;
        NotificationType:string;
        NotificationHeader:string;
        NotificationDetail:string;
        LocationName:string;
        Address:string;
        NotificationTimestamp:string;
        NotificationActionLabel:string;
        NotificationAction:string;
        NotificationImage:string;
        NotificationVideo:string;
        DeviceID:string;
        deviceType:string;
        deviceGPSLat:string;
        deviceGPSLong:string;
        

       

    }
