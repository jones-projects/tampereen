export interface IDevices {
    deviceID: string;
    deviceType: string;
    deviceGPSLat: string;
    deviceGPSLong: string;
    deviceLocationName: string;
    lastNotificationType: string;
    lastNotificationMessage: string;
    lastNotificationDetail: string;
    lastNotificationTimestamp: string;
    lastNotificationActionLabel: string;
    lastNotificationAction?:string;
    lastNotificationImage?:string;
    lastNotificationVideo?:string;
}
export interface IDeviceListResource{
    resourceCategory:IDeviceList[]
}
export interface IDeviceList{
    Devices:IDevices[]
}