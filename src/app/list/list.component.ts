import { Component, OnInit, Input, ViewEncapsulation, HostListener } from '@angular/core';

@Component({
    selector: 'app-list',
    templateUrl: './list.component.html',
    styleUrls: ['./list.component.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class ListComponent implements OnInit {

    public selectedListItem: any;

    @Input() listHeaderNameProp: string;
    @Input() showExpandBtnProp: boolean;
    @Input() inputDataProp: any[];
    @Input() listTypeProp: string;
    public showMediaOverlay: boolean;
    public mediaType:string;
    public clickedMediaUrl:string;
    constructor() {
        this.selectedListItem = '';
        this.showMediaOverlay = false;
    }

    ngOnInit() {
        //console.log(this.inputData)
    }

    public handleClickOnListItem(listItem: any, index: number) {
        if (this.showExpandBtnProp) {
            // Expand and collapse list item
            if (this.selectedListItem === listItem) {
                this.selectedListItem = '';
                return;
            }
            this.selectedListItem = listItem;
        }
    }
    @HostListener('document:keydown.escape', ['$event']) 
    public onKeydownEscapeHandler(event: KeyboardEvent) {
        this.closeImageOverlay();
    }
    
    public handleClickImageOverlay(imgUrl:any) {
        this.mediaType="image"
        this.clickedMediaUrl='assets/images/'+imgUrl;
        
      this.showMediaOverlay = true;
      document.body.style.overflow = 'hidden';
    }
    public handleClickVideoOverlay(videoUrl:any) {
        this.mediaType="video"
        this.clickedMediaUrl='assets/images/samplevideo.mp4';
        
      this.showMediaOverlay = true;
      document.body.style.overflow = 'hidden';
    }
    
    public closeImageOverlay() {
      this.showMediaOverlay = false;
      document.body.style.overflow = 'unset';
    }
}
