import { Component, OnInit, Input, ViewEncapsulation } from '@angular/core';

@Component({
    selector: 'app-list-item',
    templateUrl: './list.item.component.html',
    styleUrls: ['./list.item.component.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class ListItemComponent implements OnInit {

    @Input() listItemProp: any;
    @Input() showListItemProp: any;

    constructor() {
    }

    ngOnInit() {
    }

    public handleClickOnListItem(listItem: any, index: number) {
    }

}
