import { NgModule } from '@angular/core';
import {
    CheckChangesPipe,
    SetOverlayHeightPipe,
    MatchDevicePipe,
} from './pipe';


const PIPE_LIST = [
    CheckChangesPipe,
    SetOverlayHeightPipe,
    MatchDevicePipe,
];

@NgModule({
    declarations: [
        PIPE_LIST
    ],
    exports: [
        PIPE_LIST
    ],
    providers: [
    ]
})
export class PipeModule { }
