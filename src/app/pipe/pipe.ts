import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'setOverlayHeight',
    pure: true
})
export class SetOverlayHeightPipe implements PipeTransform {
    constructor() { }

    transform() {
        let clientHeight = document.body.clientHeight;
        return clientHeight;
    }
} 

@Pipe({
    name: 'deviceChagne',
    pure: true
})
export class CheckChangesPipe implements PipeTransform {
    constructor() { }

    transform() {
        let clientHeight = document.body.clientHeight;
        return clientHeight;
    }
}

@Pipe({
    name: 'matchDevice',
    pure: true
})
export class MatchDevicePipe implements PipeTransform {
    constructor() { }

    transform(device: any, item: any, deviceAsJson?: string) {
        console.log('device::', device);
        console.log('item::', item);
        console.log('deviceAsJson::', deviceAsJson);
        // selectedItemInList['DeviceID'] === list['DeviceID'] && selectedItemInList['deviceGPSLat'] === list['deviceGPSLat']
        return (device['DeviceId'] == item['DeviceID'] && device['deviceGPSLat'].includes(item['deviceGPSLat']));
    }
}