import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';



import { HeatmapComponent } from './heatmap/heatmap.component';
import { DevicesComponent } from './devices/devices.component';
import { AlertsComponent } from './alerts/alerts.component';
import { NotificationsComponent } from './notifications/notifications.component';
import { DeviceComponent } from './device/device.component';

const routes: Routes = [
    {
        path: '',
        component: DeviceComponent
    },
    {
        path: 'heatmaps',
        component: HeatmapComponent,
        children: [{
            path: '',
            component: HeatmapComponent
        },
        {
            path: 'Illumination',
            component: HeatmapComponent
        },
        {
            path: 'default',
            component: HeatmapComponent
        },
        {
            path: 'Safety_Score',
            component: HeatmapComponent
        },
        {
            path: 'Trafic_Congestio',
            component: HeatmapComponent
        },
        {
            path: 'Air_Quality',
            component: HeatmapComponent
        }]
    },
    {
        path: 'devices',
        component: DeviceComponent
    },
    {
        path: 'alerts',
        component: AlertsComponent
    },
    {
        path: 'notifications',
        component: NotificationsComponent
    },
    // {
    //     path: 'operations',
    //     component: OperationsComponent
    // }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }