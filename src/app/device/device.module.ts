import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListModule } from '../list/list.module';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { DeviceComponent } from './device.component';
import { ListItemModule } from '../list/list.item/list.item.module';
import { PipeModule } from '../pipe/pipe.module';

@NgModule({
    declarations: [
        DeviceComponent
    ],
    imports: [
        CommonModule,
        LeafletModule,
        ListModule,
        ListItemModule,
        PipeModule
    ]
})
export class DeviceModule { }
