import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { Map, MapOptions, tileLayer, TileLayer, latLng, LayerGroup, Marker, Layer } from 'leaflet';
import { HttpService } from '../service/http.service';
import { IDeviceListResource, IDevices } from '../interfaces/IDevices';
import { CustomLeafletMarkerPin } from '../service/customLeafletMarkerPin';
import { AppService } from '../app.service';

@Component({
    selector: 'app-device',
    templateUrl: './device.component.html',
    styleUrls: ['./device.component.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class DeviceComponent implements OnInit {

    private httpService: HttpService;

    public cameraList: any[];
    public filteredCameraList: any[];

    public airSensorList: IDevices[];
    public filteredAirSensorList: any[];

    public lightPoleList: IDevices[];
    public filteredLightPoleList: IDevices[];

    public map: Map;
    public mapOptions: MapOptions;

    public baseLayer: TileLayer;
    public cameraLayer: TileLayer;

    public cameraMarkerGroups: LayerGroup<any>;
    public lightPoleMarkerGroups: LayerGroup<IDevices>;
    public airSensorMarkerGroups: LayerGroup<IDevices>;

    public deviceNameList: string[];

    public selectedListToShow: any;
    public selectedItemInList: IDevices;

    constructor(private appService: AppService) {
        this.httpService = HttpService.Instance;
        this.cameraList = [];
        this.filteredCameraList = [];
        this.airSensorList = [];
        this.filteredAirSensorList = [];
        this.lightPoleList = [];
        this.filteredLightPoleList = [];
        this.deviceNameList = ["Air Sensors", "Cameras", "Light Poles", "Events", "Incidents", "Operational Alerts"];
        this.selectedItemInList = <IDevices>{};

        this.selectedListToShow = {
            cameras: true,
            lightPoleList: false,
            airSensorList: false
        }

        this.cameraMarkerGroups = new LayerGroup<any>();
        this.lightPoleMarkerGroups = new LayerGroup<IDevices>();
        this.airSensorMarkerGroups = new LayerGroup<IDevices>();

        this.baseLayer = tileLayer(
            'http://{s}.google.com/vt/lyrs=s&x={x}&y={y}&z={z}',
            {
                maxZoom: 20,
                minZoom: 4,
                attribution: 'Qualcomm-Base',
                tileSize: 512,
                zoomOffset: -1,
                subdomains: ['mt0', 'mt1', 'mt2', 'mt3']
            });

        this.cameraLayer = tileLayer(
            'http://{s}.google.com/vt/lyrs=s&x={x}&y={y}&z={z}',
            {
                maxZoom: 20,
                minZoom: 4,
                attribution: 'Camera',
                tileSize: 512,
                zoomOffset: -1,
                subdomains: ['mt0', 'mt1', 'mt2', 'mt3']
            });

        this.appService.observeDeviceList().subscribe((selectedDeviceList: any[]) => {
            console.log('selectedDeviceList::', selectedDeviceList);
            this.hideAndShowMarkerGroup(selectedDeviceList);
        })
    }

    ngOnInit() {
        this.initializeMapOptions();
    }

    public hideAndShowMarkerGroup(selectedDeviceList: any[]) {
        this.deviceNameList.forEach((deviceName) => {
            console.log('deviceName::', deviceName);
            // check deviceName is in selectedDeviceList
            let isDeviceNameInList = selectedDeviceList.find(device => device === deviceName);
            console.log('isDeviceNameInList::', isDeviceNameInList);
            if (isDeviceNameInList) {
                if (deviceName === 'Air Sensors') {
                    this.removeMarkerGroup(this.airSensorMarkerGroups);
                    this.filteredAirSensorList = [];
                } else if (deviceName === 'Cameras') {
                    this.removeMarkerGroup(this.cameraMarkerGroups);
                    this.filteredCameraList = [];
                } else if (deviceName === 'Light Poles') {
                    this.removeMarkerGroup(this.lightPoleMarkerGroups);
                    this.filteredLightPoleList = [];
                }
            } else {
                if (deviceName === 'Air Sensors') {
                    this.showAirSensorMarkerGroup();
                    this.filteredAirSensorList = this.airSensorList;
                } else if (deviceName === 'Cameras') {
                    this.showCameraMarkerGroup();
                    this.filteredCameraList = this.cameraList;
                } else if (deviceName === 'Light Poles') {
                    this.showLightPoleMarkerGroup();
                    this.filteredLightPoleList = this.lightPoleList;
                }
            }
        });
    }

    public initializeMapOptions() {
        this.mapOptions = {
            center: latLng(61.494333, 23.775472),
            zoom: 15,
            layers: [
                this.baseLayer,
                this.cameraLayer
            ],
        };
    }

    public onMapReady(map: Map) {
        this.map = map;
        this.httpService.requestPromiseGet('http://localhost:1334/assets/json/devices.json').then((devices: IDeviceListResource) => {
            this.cameraList = Object.values(devices.resourceCategory["Cameras"]);
            this.lightPoleList = Object.values(devices.resourceCategory["Light Poles"]);
            this.airSensorList = Object.values(devices.resourceCategory["Air Sensors"]);

            this.filteredCameraList = this.cameraList;
            this.filteredAirSensorList = this.airSensorList;
            this.filteredLightPoleList = this.lightPoleList;

            this.showCameraMarkerGroup();
            this.showLightPoleMarkerGroup();
            this.showAirSensorMarkerGroup();

        });
    }

    public showCameraMarkerGroup() {
        if (this.cameraMarkerGroups.getLayers().length) {
            return;
        }
        this.cameraMarkerGroups = new LayerGroup(
            this.cameraList.map(x =>
                new Marker([Number(x.deviceGPSLat), Number(x.deviceGPSLong)]
                    , { icon: new CustomLeafletMarkerPin().seletedMarkerPin(x.deviceType, x.lastNotificationType) })
                    .bindPopup(this.renderMarkerPopup(x))
                    .on('click', this.markerClick, this)
                    .addTo(this.map)
            ));
    }

    public showLightPoleMarkerGroup() {
        if (this.lightPoleMarkerGroups.getLayers().length) {
            return;
        }
        this.lightPoleMarkerGroups = new LayerGroup(
            this.lightPoleList.map(x =>
                new Marker([Number(x.deviceGPSLat), Number(x.deviceGPSLong)]
                    , { icon: new CustomLeafletMarkerPin().seletedMarkerPin(x.deviceType, x.lastNotificationType) })
                    .bindPopup(this.renderMarkerPopup(x))
                    .on('click', this.markerClick, this)
                    .addTo(this.map)
            ));
    }

    public showAirSensorMarkerGroup() {
        if (this.airSensorMarkerGroups.getLayers().length) {
            return;
        }
        this.airSensorMarkerGroups = new LayerGroup(
            this.airSensorList.map(x =>
                new Marker([Number(x.deviceGPSLat), Number(x.deviceGPSLong)]
                    , { icon: new CustomLeafletMarkerPin().seletedMarkerPin(x.deviceType, x.lastNotificationType) })
                    .bindPopup(this.renderMarkerPopup(x))
                    .on('click', this.markerClick, this)
                    .addTo(this.map)
            ));
    }

    public removeMarkerGroup(markerGroup: LayerGroup) {
        let layers = markerGroup.getLayers();
        layers.forEach((value) => {
            value.remove();
        })
        markerGroup.clearLayers();
    }

    private markerClick(event) {
        
        if (event && event.latlng) {
            // const { lat, lng } = event.latlng;
            const lat = event.latlng.lat.toString();
            const lng = event.latlng.lng.toString();

            let selectedItem = this.filteredCameraList.find((item) => item.deviceGPSLat.includes(lat) && item.deviceGPSLong.includes(lng));
            if (selectedItem) {
                this.selectedItemInList = selectedItem;
                return;
            }
            selectedItem = this.filteredAirSensorList.find((item) => item.deviceGPSLat.includes(lat) && item.deviceGPSLong.includes(lng));
            if (selectedItem) {
                this.selectedItemInList = selectedItem;
                return;
            }
            
            selectedItem = this.filteredLightPoleList.find((item) => item.deviceGPSLat.includes(lat) && item.deviceGPSLong.includes(lng));
            if (selectedItem) {
                this.selectedItemInList = selectedItem;
                return;
            }
        }

    }

    private togglePopupPinOnMap(list: IDevices, layerList: any[], showPopupFlag: boolean) {
        if (list && layerList && layerList.length) {
            
            for (let index = 0; index < layerList.length - 1; index++) {
                const mapMarkerLayers = layerList[index];
                let { lat, lng } = mapMarkerLayers.getLatLng();
                
                if (list.deviceGPSLat == lat && list.deviceGPSLong == lng) {
                    if (showPopupFlag) {
                        mapMarkerLayers.openPopup();
                    } else {
                        mapMarkerLayers.closePopup();
                    }
                }
            }

        }
    }

    public handleClickOnListItem(list: IDevices, selectedList?: string) {
        let layerList;
        if (list.deviceType === 'Camera') {
            layerList = this.cameraMarkerGroups.getLayers();
        } else if (list.deviceType === 'Light Poles') {
            layerList = this.lightPoleMarkerGroups.getLayers();
        } else if (list.deviceType === 'Air Sensors') {
            layerList = this.airSensorMarkerGroups.getLayers();
        }
        if (this.selectedItemInList['DeviceID'] !== list['DeviceID']) {
            this.togglePopupPinOnMap(list, layerList, true);
            this.selectedItemInList = list;
            return;
        }
        
        this.togglePopupPinOnMap(list, layerList, false);
        this.selectedItemInList = <IDevices>{}
    }

    public expandCollapseList(item) {
        this.selectedListToShow[item] = !this.selectedListToShow[item];
        for (let listItem in this.selectedListToShow) {
            this.selectedListToShow[listItem] = (listItem === item);
        }
    }

    private renderMarkerPopup(dev: any) {
        return `
            <section class="img-name-and-btn">
                <div class="icnwarning">
                    <span class="material-icons">warning</span> 
                </div>
                <label class="list-item-name map">${dev.deviceLocationName}</label>
                <div class="list-item-image">
                    <span class="material-icons">videocam</span>
                </div>
                <div class="list-item-image">
                    <span class="material-icons">photo_library</span>
                </div>
            </section>
            <section class="list-item-info">
                <header>${dev.lastNotificationMessage}</header>
                <address>${dev.lastNotificationDetail}</address>
                <aside>
                    <button>${dev.lastNotificationActionLabel}</button>
                    <time>${dev.lastNotificationTimestamp}</time>
                </aside>
            </section>
        `;
    }

}